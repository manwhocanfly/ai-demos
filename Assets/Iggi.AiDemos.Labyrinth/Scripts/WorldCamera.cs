﻿using UnityEngine;
using System.Collections;
using Unity.Mathematics;

namespace Iggi.AiDemos.Labyrinth
{
    [RequireComponent(typeof(Camera))]
    public class WorldCamera : MonoBehaviour
    {
        public Camera targetCamera;

        [SerializeField, Range(0f, 1f)]
        private float lerpRate = 0.3f;

        private float targetZoom;
        private float currentZoom;

        private float2 targetPosition;
        private float2 currentPosition;

        private void Awake()
        {
            targetZoom = currentZoom = targetCamera.orthographicSize;
        }

        private void Update()
        {
            currentZoom = math.lerp(currentZoom, targetZoom, lerpRate);
            currentPosition = math.lerp(currentPosition, targetPosition, lerpRate);

            targetCamera.orthographicSize = currentZoom;
            transform.localPosition = new Vector3(currentPosition.x, currentPosition.y, -10f);
        }

        public void Zoom(World world, Vector2 screenPivot, float distance)
        {
            var previousZoom = targetZoom;
            var minMaxZoom = GetMinMaxZoom(world);
            var mouseWorldPosition = ((float3)targetCamera.ScreenToWorldPoint(screenPivot)).xy;

            targetZoom += previousZoom * distance * 0.2f;
            targetZoom = math.clamp(targetZoom, minMaxZoom.x, minMaxZoom.y);

            var bounds = GetBounds(world);
            targetPosition = mouseWorldPosition + (targetPosition - mouseWorldPosition) * (targetZoom / previousZoom);
            targetPosition = math.clamp(targetPosition, bounds.xy, bounds.zw);
        }

        public void ResetZoom(World world)
        {
            var minMaxZoom = GetMinMaxZoom(world);

            targetZoom = currentZoom = minMaxZoom.y;
            targetPosition = currentPosition = GetMapCenter(world);

            Update();
        }

        public float2 GetMinMaxZoom(World world)
        {
            return new float2(5f, math.max(world.Map.Size.x / targetCamera.aspect, world.Map.Size.y) / 2f + 10f); 
        }

        public float2 GetMapCenter(World world)
        {
            return (float2)world.Map.Size / 2f;
        }

        public float4 GetBounds(World world)
        {
            var center = GetMapCenter(world);
            var bounds = math.max(0f, world.Map.Size - targetZoom * 2f * new float2(targetCamera.aspect, 1f));
            return new float4(-bounds.x, -bounds.y, bounds.x, bounds.y) + center.xyxy;
        }

        public bool TryGetMouseTilePosition(World world, out int2 tilePosition)
        {
            if (world.Map == null || world.Grid == null)
            {
                tilePosition = default;
                return false;
            }

            var mouseWorldPosition = targetCamera.ScreenToWorldPoint(Input.mousePosition);
            var cell = world.Grid.WorldToCell(mouseWorldPosition);

            tilePosition = new int2(cell.x, cell.y);

            return world.Map.Contains(tilePosition);
        }
    }
}

