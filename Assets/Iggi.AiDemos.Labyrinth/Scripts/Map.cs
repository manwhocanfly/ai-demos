﻿using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.Tilemaps;
using Unity.Mathematics;

namespace Iggi.AiDemos.Labyrinth
{
    public enum MapTileType : byte
    {
        None = 0,
        Floor = 1,
        Wall = 2,
    }

    [RequireComponent(typeof(Tilemap))]
    public class Map : MonoBehaviour
    {
        #region Unity Inspector Properties

        [SerializeField] private Tilemap tilemap = null;
        [SerializeField] private SpriteRenderer border = null;
        [SerializeField] private TileBase[] floorTiles = new TileBase[0];
        [SerializeField] private TileBase[] wallTiles = new TileBase[0];

        #endregion

        public int2 Size { get; private set; }
        public MapTileType[] Tiles { get; private set; }
        public MapPathfinder Pathfinder { get; private set; }
        public Tilemap Tilemap => tilemap;

        public float p_priorityMul = 10f;
        public float p_heuristicWeight = 10f;
        public float p_costWeight = 10f;

        private void Awake()
        {
            Pathfinder = new MapPathfinder(this);
            UpdateMapBorder();
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public bool Contains(int2 localPosition)
        {
            return localPosition.x >= 0 && localPosition.x < Size.x && localPosition.y >= 0 && localPosition.y < Size.y;
        }

        public MapTileType GetTile(int2 localPosition)
        {
            if (!Contains(localPosition)) return default;

            var id = PositionToID(localPosition);

            return Tiles[id];
        }

        public void SetTile(int2 localPosition, MapTileType tileType)
        {
            if (!Contains(localPosition)) return;

            var id = PositionToID(localPosition);

            Tiles[id] = tileType;

            RedrawTile(localPosition);
        }

        public void SetTiles(int2 size, MapTileType[] tiles)
        {
            Size = size;
            Tiles = tiles;

            UpdateMapBorder();
            RedrawAllTiles();
        }

        public bool TryGetRandomFloorTile(out int2 tilePosition)
        {
            var limit = 10000;

            tilePosition = default;

            if (Tiles == null) return false;

            while(--limit > 0)
            {
                var randomID = UnityEngine.Random.Range(0, Tiles.Length);

                if (Tiles[randomID] == MapTileType.Floor)
                {
                    tilePosition = IDToPosition(randomID);
                    return true;
                }
            }

            return false;
        }

        private void UpdateMapBorder()
        {
            if (math.lengthsq(Size) > 0)
            {
                var scale = border.transform.localScale.x;

                border.gameObject.SetActive(true);
                border.transform.localPosition = new Vector3(Size.x / 2f, Size.y / 2f);
                border.size = new Vector2(Size.x / scale + 0.07f, Size.y / scale + 0.07f);
            }
            else
            {
                border.gameObject.SetActive(false);
            }
        }

        private void RedrawAllTiles()
        {
            if (tilemap == null) return;

            tilemap.ClearAllTiles();

            for (int x = 0; x < Size.x; x++)
            {
                for (int y = 0; y < Size.y; y++)
                {
                    RedrawTile(new int2(x, y));
                }
            }
        }

        private void RedrawTile(int2 position)
        {
            if (!Contains(position) || tilemap == null) return;

            var tileType = GetTile(position);

            var tileset = (tileType == MapTileType.Floor) ? floorTiles : wallTiles;

            if (tileset.Length == 0) return;

            var tile = (position.x + position.y) % tileset.Length;

            tilemap.SetTile(new Vector3Int(position.x, position.y, 0), tileset[tile]);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private int2 IDToPosition(int id) => new int2(id % Size.x, id / Size.x);

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private int PositionToID(int2 position) => position.x + position.y * Size.x;
    }
}
