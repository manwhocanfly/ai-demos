﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using Unity.Mathematics;
using System;
using UnityEngine.EventSystems;
using System.Collections.Generic;

namespace Iggi.AiDemos.Labyrinth
{
    public class World : StateMachine
    {
        #region Unity Inspector Properties

        [Header("UI")]
        [SerializeField] private GameObject uiWindowGenerateMap = null;
        [SerializeField] private Button buttonBack = null;
        [SerializeField] private Button buttonGenerateMap = null;
        [SerializeField] private InputField inputMapWidth = null;
        [SerializeField] private InputField inputMapHeight = null;
        [SerializeField] private Slider inputMapSize = null;

        [Header("Game Objects")]
        [SerializeField] private WorldCamera worldCamera = null;
        [SerializeField] private Grid grid = null;
        [SerializeField] private Map map = null;
        [SerializeField] private Turtle turtlePrefab = null;

        #endregion

        public override State DefaultState => gameObject.GetOrAddComponent<GenerateMapState>();

        private InputSystem inputSystem = new InputSystem();

        public Turtle Turtle { get; private set; } = null;
        public Map Map => map;
        public WorldCamera Camera => worldCamera;
        public Grid Grid => grid;

        private void Awake()
        {
            uiWindowGenerateMap.gameObject.SetActive(false);
            map.gameObject.SetActive(false);

            buttonGenerateMap.onClick.AddListener(InitializeMap);
            buttonBack.onClick.AddListener(GoBack);
            inputMapSize.onValueChanged.AddListener(OnInputMapSizeChange);
        }

        protected override void Update()
        {
            base.Update();

            inputSystem.Update(this);
        }

        public void GoBack()
        {
            if(CurrentState is PlayState)
            {
                GotoState<GenerateMapState>();
            }
            else
            {
                UnityEngine.SceneManagement.SceneManager.LoadScene(0);
            }
        }

        private void OnInputMapSizeChange(float value)
        {
            var size = Mathf.RoundToInt(value);

            inputMapWidth.text = $"{size}";
            inputMapHeight.text = $"{size}";
        }

        private void InitializeMap()
        {
            if (!int.TryParse(inputMapWidth.text, out int mapWidth))
            {
                EventSystem.current.SetSelectedGameObject(inputMapWidth.gameObject);
                return;
            }

            if (!int.TryParse(inputMapHeight.text, out int mapHeight))
            {
                EventSystem.current.SetSelectedGameObject(inputMapHeight.gameObject);
                return;
            }

            var mapSize = new int2(Mathf.RoundToInt(inputMapSize.value), Mathf.RoundToInt(inputMapSize.value));

            // Generate Map Layout
            using (var mazeGenerator = new MazeGenerator())
            {
                var tiles = mazeGenerator.GenerateTiles(mapSize);
                map.SetTiles(mapSize, tiles);
            }

            // Spawn agent
            if (!Turtle && turtlePrefab)
            {
                Turtle = Instantiate(turtlePrefab, transform, true);
                Turtle.OnAddedToWorld(this);
            }

            if (Map.TryGetRandomFloorTile(out int2 unitSpawnPosition))
            {
                Turtle.MoveTo(unitSpawnPosition, true);
            }

            // Reset Camera
            worldCamera.ResetZoom(this);

            GotoState<PlayState>();
        }

        #region StateMachine States

        public abstract class WorldState : State
        {
            public World World => (World)StateMachine;
        }

        public class GenerateMapState : WorldState
        {
            public override void OnStateStart()
            {
                if (World.uiWindowGenerateMap)
                {
                    World.uiWindowGenerateMap.gameObject.SetActive(true);
                }
            }

            public override void OnStateEnd()
            {
                if(World.uiWindowGenerateMap)
                {
                    World.uiWindowGenerateMap.gameObject.SetActive(false);
                }
            }
        }

        public class PlayState : WorldState
        {
            public override void OnStateStart()
            {
                if (World.Map) World.Map.gameObject.SetActive(true);
                if (World.Turtle) World.Turtle.gameObject.SetActive(true);
            }

            public override void OnStateEnd()
            {
                if (World.Map) World.Map.gameObject.SetActive(false);
                if (World.Turtle) World.Turtle.gameObject.SetActive(false);
            }
        }

        #endregion
    }
}
