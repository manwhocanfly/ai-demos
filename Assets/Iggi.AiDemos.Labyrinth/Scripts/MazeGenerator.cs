﻿using System;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;
using RNG = UnityEngine.Random;

namespace Iggi.AiDemos.Labyrinth
{
    public class MazeGenerator : IDisposable
    {
        public struct FloorMaker
        {
            public int2 position;
            public int direction;
            public float deathChance;
            public float splitChance;
        }

        private float turnChance = 0.2f;
        private int[] turnChances = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, -1 };
        private int2[] directions = new int2[] { new int2(-1, 0), new int2(1, 0), new int2(0, -1), new int2(0, 1) };
        private float floorsTargetPct = 0.4f;
        private float floorMakerDecay = 0.0001f;
        private float floorMakerSplitChance = 0.08f;
        private int maxFloorMakers = 20;

        private List<FloorMaker> floorMakers = new List<FloorMaker>();
        private HashSet<int2> floors = new HashSet<int2>();

        private int2 effectiveSize = default;
        private int minFloorCount = default;

        public MapTileType[] GenerateTiles(int2 mapSize)
        {
            var turnChanceCount = turnChances.Length;

            effectiveSize = mapSize / 4;
            minFloorCount = Mathf.RoundToInt(effectiveSize.x * effectiveSize.y * 4 * floorsTargetPct);

            floorMakers.Clear();
            floors.Clear();

            // Add start floor makers
            for (int i = 0; i < 4; i++) floorMakers.Add(new FloorMaker() { direction = i, splitChance = floorMakerSplitChance });

            // Update floor makers
            while (floors.Count < minFloorCount && floorMakers.Count > 0)
            {
                for (int i = 0; i < floorMakers.Count; i++)
                {
                    var maker = floorMakers[i];

                    if(floors.Contains(maker.position))
                    {
                        maker.deathChance += floorMakerDecay * 2f;
                    }
                    else
                    {
                        floors.Add(maker.position);
                    }

                    if (RNG.value < turnChance)
                    {
                        maker.direction += RNG.Range(-1, 4) + 4;
                    }

                    maker.position += directions[maker.direction % 4];
                    maker.deathChance += floorMakerDecay;
                    maker.splitChance += floorMakerDecay;

                    floorMakers[i] = maker;

                    if (maker.position.x < -effectiveSize.x || maker.position.x > effectiveSize.x ||
                        maker.position.y < -effectiveSize.y || maker.position.y > effectiveSize.y ||
                        RNG.value < maker.deathChance
                    ) {
                        floorMakers.RemoveAt(i--);
                        continue;
                    }
                    
                    if(floorMakers.Count < maxFloorMakers && RNG.value < maker.splitChance )
                    {
                        floorMakers.Add(new FloorMaker() { position = maker.position, direction = RNG.Range(0, 4), splitChance = floorMakerSplitChance });
                    }
                }
            }

            var tiles = new MapTileType[mapSize.x * mapSize.y];

            // Apply floor makers
            for (int x = 0; x < mapSize.x; x++)
            {
                for (int y = 0; y < mapSize.y; y++)
                {
                    var pos = new int2(x, y) / 2 - effectiveSize;
                    var id = x + y * mapSize.x;

                    tiles[id] = floors.Contains(pos) ? MapTileType.Floor : MapTileType.Wall;
                }
            }

            // Corode walls
            for (int x = 1; x < mapSize.x - 1; x++)
            {
                for (int y = 1; y < mapSize.y - 1; y++)
                {
                    var id = x + y * mapSize.x;
                    var nCount = 0;

                    if (tiles[id] == MapTileType.Floor) continue;

                    for (int i = 0; i < 4; i++)
                    {
                        var nPos = new int2(x, y) + directions[i];
                        var nId = nPos.x + nPos.y * mapSize.x;

                        if (tiles[nId] == MapTileType.Wall) ++nCount;
                    }

                    if((nCount == 2 && RNG.value < 0.5f) || (nCount == 3 && RNG.value < 0.1f))
                    {
                        tiles[id] = MapTileType.Floor;
                    }
                }
            }

            return tiles;
        }
        
        public void Dispose()
        {
            turnChances = null;
            directions = null;
            floorMakers = null;
            floors = null;
        }
    }
}
