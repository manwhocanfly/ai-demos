﻿using Demo;
using System;
using System.Runtime.CompilerServices;
using Unity.Mathematics;

namespace Iggi.AiDemos.Labyrinth
{
    /// <summary>
    /// Basic but fast implementation of A* algorithm by Iggi
    /// </summary>
    public class MapPathfinder
    {
        private struct PathfinderNode
        {
            public int id;
            public byte status;
            public float cost;
        }

        public const int MAX_PATH_LENGTH = 512;

        private readonly int2[] neighbours = new int2[] 
        {
            new int2(-1, 0),
            new int2(1, 0),
            new int2(0, -1),
            new int2(0, 1),
        };

        private PriorityQueue queue;
        private Map map;
        private int2 mapSize;
        private float maxCost;

        private PathfinderNode[] nodes = new PathfinderNode[0];
        private byte openNodeValue;
        private byte closeNodeValue;

        public MapPathfinder(Map map)
        {
            this.map = map;

            queue = new PriorityQueue(MAX_PATH_LENGTH * MAX_PATH_LENGTH);
            openNodeValue = 1;
            closeNodeValue = 2;
        }

        private void PrepareMap()
        {
            if (map == null || map.Size.Equals(default) || map.Size.Equals(mapSize)) return;

            mapSize = map.Size;
            Array.Resize(ref nodes, mapSize.x * mapSize.y);
        }

        private bool CalculatePath(int start, int end)
        {
            var id = start;
            var startPos = IDToPosition(start);
            var endPos = IDToPosition(end);
            var found = false;

            maxCost = 0f;
            openNodeValue += 2;
            closeNodeValue += 2;

            queue.Clear();

            nodes[id].id = id;
            nodes[id].cost = 0;
            nodes[id].status = openNodeValue;

            queue.Enqueue(id, 0);

            while (queue.Size > 0)
            {
                id = queue.Dequeue();

                var idPos = IDToPosition(id);

                // Is it in closed list? means this node was already processed
                if (nodes[id].status == closeNodeValue) continue;

                // Is this the end
                if (id == end)
                {
                    nodes[id].status = closeNodeValue;
                    found = true;
                    break;
                }

                var nextCost = nodes[id].cost + 1;
                if (maxCost < nextCost) maxCost = nextCost;

                // Go trough each neighbour
                for (var link = 0; link < 4; link++)
                {
                    var newIdPos = idPos + neighbours[link];

                    // if not walkable or out of bounds, skip it
                    if (map.GetTile(newIdPos) != MapTileType.Floor)
                    {
                        continue;
                    }

                    var newId = PositionToID(newIdPos);

                    // The current node has less cost than the previous? then skip this node
                    if ((nodes[newId].status == openNodeValue || nodes[newId].status == closeNodeValue) && nodes[newId].cost <= nextCost)
                    {
                        continue;
                    }

                    // Simple squared Euclidean distance
                    var heuristic = math.lengthsq(newIdPos - endPos);

                    nodes[newId].id = id;
                    nodes[newId].cost = nextCost;
                    nodes[newId].status = openNodeValue;

                    if (!queue.Enqueue(newId, nextCost * nextCost + heuristic))
                    {
                        // If enqueuing fails return null path
                        return false;
                    }
                }

                nodes[id].status = closeNodeValue;
            }

            return found;
        }

        public int FindPathNonAlloc(int2 startPosition, int2 endPosition, int2[] path)
        {
            // If start or end are outside the tilemap return nothing
            if (!map.Contains(startPosition) || !map.Contains(endPosition) || map.Size.Equals(default)) return 0;

            PrepareMap();

            var start = PositionToID(startPosition);
            var end = PositionToID(endPosition);
            var pathLength = path.Length;
            var pathStepCount = 0;
            var tempId = end;

            var found = CalculatePath(start, end);

            // Create and return path
            if (found && pathLength > 0)
            {
                path[pathStepCount] = endPosition;
                pathStepCount++;

                while (tempId != start && pathStepCount < pathLength)
                {
                    tempId = nodes[tempId].id;
                    path[pathStepCount] = IDToPosition(tempId);
                    pathStepCount++;
                }
            }

            // Reverse path
            for (int i = 0; i < pathStepCount / 2; i++)
            {
                var temp = path[i];
                path[i] = path[pathStepCount - 1 - i];
                path[pathStepCount - 1 - i] = temp;
            }

            return pathStepCount;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private int2 IDToPosition(int id) => new int2(id % mapSize.x, id / mapSize.x);

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private int PositionToID(int2 position) => position.x + position.y * mapSize.x;
    }
}
