﻿using UnityEngine;
using System.Collections;
using Unity.Mathematics;
using System;

namespace Iggi.AiDemos.Labyrinth
{
    /// <summary>
    /// Turtle is a simple unit that walks across map
    /// 
    /// States:
    /// <see cref="Turtle.IdleState"/>
    /// <see cref="Turtle.FindTargetState"/>
    /// <see cref="Turtle.MoveAlongPathState"/>
    /// </summary>
    public class Turtle : StateMachine
    {
        #region Unity Inspector Properties

        [SerializeField] private SpriteRenderer locationMarker = null;
        [SerializeField] private SpriteRenderer helloMessage = null;
        [SerializeField] private TrailRenderer pathTrail = null;

        #endregion

        public override State DefaultState => gameObject.GetOrAddComponent<IdleState>();

        private int2[] path = new int2[MapPathfinder.MAX_PATH_LENGTH];
        private int pathLength = 0;

        public World World { get; private set; }
        public int2 LocalTilePosition { get; private set; }
        public int2? TargetTilePosition { get; private set; }

        /// <summary>
        /// Find the optimal path to target and start moving towards it
        /// </summary>
        /// <param name="targetPosition"></param>
        /// <param name="instantly"></param>
        public void MoveTo(int2 targetPosition, bool instantly = false)
        {
            if(instantly)
            {
                LocalTilePosition = targetPosition;
                transform.position = (Vector2)(LocalTilePosition + new float2(0.5f, 0.5f));
                pathTrail.Clear();
            }
            else if (TrySetTargetPosition(targetPosition))
            {
                GotoState<MoveAlongPathState>();
            }
            else
            {
                ClearTargetPosition();
                GotoState<IdleState>();
            }
        }

        public bool TryGetNextPositionOnPath(out int2 position)
        {
            for (int i = 0; pathLength > 0 && i < pathLength - 1; i++)
            {
                if (LocalTilePosition.Equals(path[i]))
                {
                    position = path[i + 1];
                    return true;
                }
            }

            position = default;
            return false;
        }

        public bool TrySetTargetPosition(int2 targetPosition)
        {
            if (World.Map == null) return false;
            if (World.Map.GetTile(targetPosition) != MapTileType.Floor) return false;

            pathLength = World.Map.Pathfinder.FindPathNonAlloc(LocalTilePosition, targetPosition, path);

            if (pathLength > 0)
            {
                TargetTilePosition = targetPosition;
                return true;
            }
            else
            {
                TargetTilePosition = null;
                return false;
            }
        }

        public void ClearTargetPosition()
        {
            TargetTilePosition = null;
            pathLength = 0;
        }

        public void OnAddedToWorld(World world)
        {
            World = world;
            gameObject.SetActive(true);
        }

        private void LateUpdate()
        {
            // If this unit is instantiated but not added in any world we disable it
            if (World == null)
            {
                gameObject.SetActive(false);
                return;
            }

            transform.position = (Vector2)(LocalTilePosition + new float2(0.5f, 0.5f));

            ApplyDynamicOpacity(locationMarker, 20f, 50f);
            ApplyDynamicOpacity(helloMessage, 10f, 5f);
        }

        private void ApplyDynamicOpacity(SpriteRenderer spriteRenderer, float minZoom, float maxZoom)
        {
            var color = spriteRenderer.color;
            color.a = Mathf.Lerp(0f, 1f, Mathf.InverseLerp(minZoom, maxZoom, World.Camera.targetCamera.orthographicSize));
            spriteRenderer.color = color;
        }

        #region StateMachine States

        public abstract class TurtleState : State
        {
            public Turtle Turtle => (Turtle)StateMachine;
        }

        /// <summary>
        /// Waits for X seconds then gets bored and starts searching for a new target
        /// </summary>
        public class IdleState : TurtleState
        {
            public override void OnStateUpdate()
            {
                if (Timer > 8f && Turtle.World.Map)
                {
                    Turtle.GotoState<FindTargetState>();
                }
            }
        }

        /// <summary>
        /// Finds random target position and starts moving towards it
        /// </summary>
        public class FindTargetState : TurtleState
        {
            public override void OnStateStart()
            {
                if (Turtle.World.Map.TryGetRandomFloorTile(out int2 targetPosition) && Turtle.TrySetTargetPosition(targetPosition))
                {
                    Turtle.GotoState<MoveAlongPathState>();
                }
                else
                {
                    Turtle.GotoState<IdleState>();
                }
            }
        }

        /// <summary>
        /// If the unit has clear path to target, move towards it
        /// </summary>
        public class MoveAlongPathState : TurtleState
        {
            public override void OnStateUpdate()
            {
                // Check if we arrived at destination
                if (Turtle.TryGetNextPositionOnPath(out int2 position))
                {
                    Turtle.LocalTilePosition = position;
                }
                else
                {
                    Turtle.ClearTargetPosition();
                    Turtle.GotoState<IdleState>();
                }
            }
        }

        #endregion
    }


}
