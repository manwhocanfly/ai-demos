﻿using UnityEngine;
using System.Collections;
using Unity.Mathematics;

namespace Iggi.AiDemos.Labyrinth
{
    /// <summary>
    /// Input controls for Demo 2
    /// 
    /// - [Escape] Go back to previous screen
    /// - [Left Mouse Button] Move unit
    /// - [Right Mouse Button] Switch Tile
    /// - [Mouse scroll] Zoom
    /// </summary>
    public class InputSystem
    {
        MapTileType? brushType = null;

        public void Update(World world)
        {
            // Escape - Go Back
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                world.GoBack();
            }

            if (world.CurrentState is World.PlayState)
            {
                // Left Mouse Button - Move unit
                if (Input.GetMouseButtonDown(0) && world.Camera.TryGetMouseTilePosition(world, out int2 targetTile))
                {
                    world.Turtle.MoveTo(targetTile);
                }


                // Right Mouse Button - Switch Tile
                if (Input.GetMouseButton(1))
                {
                    if (world.Camera.TryGetMouseTilePosition(world, out targetTile))
                    {
                        var currentTile = world.Map.GetTile(targetTile);

                        if (brushType == null)
                        {
                            brushType = (currentTile == MapTileType.Floor) ? MapTileType.Wall : MapTileType.Floor;
                        }

                        if (currentTile != brushType.Value)
                        {
                            world.Map.SetTile(targetTile, brushType.Value);

                            // Recalculate turtle path
                            if (world.Turtle.TargetTilePosition.HasValue)
                            {
                                world.Turtle.MoveTo(world.Turtle.TargetTilePosition.Value);
                            }
                        }
                    }
                }
                else
                {
                    brushType = null;
                }


                // Mouse scroll - Zoom
                if (Input.mouseScrollDelta.y != 0f)
                {
                    world.Camera.Zoom(world, Input.mousePosition, -Input.mouseScrollDelta.y);
                }
            }
        }

        
    }
}
