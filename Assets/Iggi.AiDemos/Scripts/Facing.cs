﻿using Unity.Mathematics;
using UnityEngine;

namespace Iggi.AiDemos
{
    public enum Facing
    {
        Right,
        Left,
    }

    public static class FacingUtility
    {
        public static Facing GetOpposite(this Facing facing)
        {
            return facing == Facing.Right ? Facing.Left : Facing.Right;
        }

        public static int GetSign(this Facing facing)
        {
            return facing == Facing.Left ? -1 : 1;
        }

        public static int2 GetDirection(this Facing facing)
        {
            return new int2(facing == Facing.Left ? -1 : 1, 0);
        }

        public static float GetAngle(this Facing facing, float angle, float mirrorPivot = 0f, float normalizePivot = 0f)
        {
            angle = facing == Facing.Right ? angle : MathUtil.MirrorAngle(angle, mirrorPivot);
            return MathUtil.NormalizeAngle(angle, normalizePivot);
        }

        public static float GetFloat(this Facing facing, float value)
        {
            return (facing == Facing.Left ? -1 : 1) * value;
        }

        public static Vector3 GetVector(this Facing facing, Vector3 vector)
        {
            return new Vector3((facing == Facing.Left ? -1 : 1) * vector.x, vector.y, vector.z);
        }

        public static Vector2 GetVector(this Facing facing, Vector2 vector)
        {
            return new Vector2((facing == Facing.Left ? -1 : 1) * vector.x, vector.y);
        }

        public static Facing GetFromDirection(float2 direction)
        {
            return direction.x < 0f ? Facing.Left : Facing.Right;
        }
        public static T Choose<T>(this Facing facing, T left, T right)
        {
            return facing == Facing.Left ? left : right;
        }
    }
}