using UnityEngine;

namespace Iggi.AiDemos
{
    /// <summary>
    /// Basic but super-fast implementation of fixed-size priority-queue by Iggi
    /// </summary>
    public class PriorityQueue
    {
        private struct QueueNode
        {
            public int id;
            public float priority;
        }

        private readonly QueueNode[] queue;
        public readonly int capacity;

        public int Size { get; private set; } = 0;

        public PriorityQueue(int capacity)
        {
            this.capacity = capacity;
            queue = new QueueNode[capacity];
        }

        public int Dequeue()
        {
            // If queue is empty return negative value
            if (Size == 0) return -1;

            var returnId = queue[0].id;

            --Size;

            // If it was last element in queue
            if (Size == 0)
            {
                return returnId;
            }

            queue[0].id = queue[Size].id;
            queue[0].priority = queue[Size].priority;

            var indexNew = 0;

            while (true)
            {
                var indexOld = indexNew;

                var indexLeft = (indexNew << 1) + 1;
                var indexRight = (indexNew << 1) + 2;

                if (Size >= indexLeft && queue[indexNew].priority > queue[indexLeft].priority)
                {
                    indexNew = indexLeft;
                }

                if (Size >= indexRight && queue[indexNew].priority > queue[indexRight].priority)
                {
                    indexNew = indexRight;
                }

                if (indexNew == indexOld)
                {
                    break;
                }

                var temp = queue[indexNew];

                queue[indexNew] = queue[indexOld];
                queue[indexOld] = temp;
            }

            return returnId;
        }

        public bool Enqueue(int id, float priority)
        {
            // If queue is too large return false
            if (Size == capacity) return false;

            var indexNew = Size;

            queue[Size].id = id;
            queue[Size].priority = priority;

            ++Size;

            while (indexNew > 0)
            {
                var indexOld = (indexNew - 1) >> 1;

                if (queue[indexNew].priority < queue[indexOld].priority)
                {
                    var temp = queue[indexNew];

                    queue[indexNew] = queue[indexOld];
                    queue[indexOld] = temp;

                    indexNew = indexOld;
                }
                else
                {
                    break;
                }
            }

            return true;
        }

        public void Clear()
        {
            Size = 0;
        }
    }
}