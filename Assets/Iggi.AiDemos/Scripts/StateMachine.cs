﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Iggi.AiDemos
{
    public abstract class StateMachine : MonoBehaviour
    {
        public abstract class State : MonoBehaviour
        {
            public StateMachine StateMachine => GetComponent<StateMachine>();
            public float Timer => StateMachine ? StateMachine.TimeSinceLastStateChange : 0f;

            public virtual void OnStateStart() { }
            public virtual void OnStateEnd() { }
            public virtual void OnStateUpdate() { }
        }

        public delegate void StateChangeCallback();

        private float lastStateChangeTime;

        public event StateChangeCallback OnStateChange;
        public float TimeSinceLastStateChange => Time.time - lastStateChangeTime;
        public State PreviousState { get; private set; }
        public State CurrentState { get; private set; }
        public virtual State DefaultState { get; }

        public void GotoState<T>() where T : State
        {
            var state = GetComponent<T>() ?? gameObject.AddComponent<T>();
            GotoState(state);
        }

        public void GotoState(State state)
        {
            if (state && state.StateMachine != this)
            {
                throw new Exception($"State ({typeof(State).FullName}) must be on the same GameObject as StateMachine");
            }

            if (!gameObject) return;

            if (CurrentState && CurrentState.gameObject) CurrentState.OnStateEnd();

            PreviousState = CurrentState;
            CurrentState = state;
            lastStateChangeTime = Time.time;

            if (CurrentState && CurrentState.gameObject) CurrentState.OnStateStart();

            OnStateChange?.Invoke();
        }

        protected virtual void OnEnable()
        {
            if (DefaultState)
            {
                GotoState(DefaultState);
            }
        }

        protected virtual void OnDisable()
        {
            GotoState(null);
        }

        protected virtual void Update()
        {
            if (CurrentState) CurrentState.OnStateUpdate();
        }
    }
}