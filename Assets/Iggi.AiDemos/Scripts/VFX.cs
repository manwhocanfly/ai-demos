﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class VFX : MonoBehaviour
{
    #region Unity Inspector Properties

    [SerializeField] private List<ParticleSystem> particleSystems = new List<ParticleSystem>();
    [SerializeField] private List<SpriteRenderer> spriteRenderers = new List<SpriteRenderer>();
    [SerializeField] private float duration = 0f;

    #endregion

    private float startTime;

    private void Awake()
    {
        startTime = Time.time;
    }

    private void Update()
    {
        if(Time.time > (startTime + duration))
        {
            Destroy(gameObject);
        }
    }

    public static VFX Create(VFX prefab, Vector3 position, float scale = 1f, Color? color = null)
    {
        if (prefab == null) return null;

        var vfx = Instantiate(prefab, position, Quaternion.identity);

        vfx.transform.localScale *= scale;

        if(color.HasValue)
        {
            vfx.SetColor(color.Value);
        }

        return vfx;
    }

    public void SetColor(Color color)
    {
        foreach (var spriteRenderer in spriteRenderers)
        {
            spriteRenderer.color = color;
        }

        foreach (var particleSystem in particleSystems)
        {
            particleSystem.GetComponent<Renderer>().material.color = color;
        }
    }
}
