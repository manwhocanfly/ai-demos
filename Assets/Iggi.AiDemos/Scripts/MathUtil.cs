﻿using UnityEngine;
using System.Collections;
using Unity.Mathematics;

namespace Iggi.AiDemos
{
    public static class MathUtil
    {
        /// <summary>
        /// Normalized angle between <paramref name="angleAxis"/> and <paramref name="angleAxis"/> + 360 degrees
        /// </summary>
        public static float NormalizeAngle(float angle, float angleAxis = 0f)
        {
            return ((((angle - angleAxis) % 360f) + 360f) % 360f) + angleAxis;
        }

        public static float MirrorAngle(float angle, float angleAxis = 0f)
        {
            return ((angle - angleAxis) * -1f) + angleAxis;
        }

        public static bool CloseEnough(float a, float b, float tolerance)
        {
            return Mathf.Abs(a - b) <= tolerance;
        }

        public static float VectorToAngle(Vector2 vector)
        {
            return Mathf.Atan2(vector.y, vector.x) * Mathf.Rad2Deg;
        }

        public static Vector2 AngleToVector(float angle)
        {
            angle = angle * Mathf.Deg2Rad;
            return new Vector2(Mathf.Cos(angle), Mathf.Sin(angle));
        }
    }
}