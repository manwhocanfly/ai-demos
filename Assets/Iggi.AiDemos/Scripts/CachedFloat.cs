﻿using UnityEngine;
using System.Collections;

namespace Iggi.AiDemos
{
    public struct CachedFloat
    {
        private float value;
        private bool changed;

        public float Value
        {
            get => value;
            set
            {
                if (Mathf.Approximately(this.value, value)) return;

                this.value = value;
                changed = true;
            }
        }

        public bool Changed
        {
            get
            {
                if (changed)
                {
                    changed = false;
                    return true;
                }

                return false;
            }
        }

        public CachedFloat(float value)
        {
            this.value = value;
            changed = false;
        }

        public static implicit operator float(CachedFloat cachedFloat) => cachedFloat.Value;
        public static implicit operator CachedFloat(float value) => new CachedFloat(value);
    }
}
