﻿using UnityEngine;
using System.Collections;
using Unity.Mathematics;

namespace Iggi.AiDemos
{
    public struct SpringFloat
    {
        private float target;

        public float Offset { get; set; }
        public float Velocity { get; set; }
        public float Dampening { get; set; }
        public float Friction { get; set; }

        public float Value
        {
            get
            {
                return target + Offset;
            }
            set
            {
                target = value - Offset;
            }
        }

        public float Target
        {
            get
            {
                return target;
            }
            set
            {
                Offset -= value - target;
                target = value;
            }
        }

        public SpringFloat(float target = 0f, float offset = 0f, float velocity = 0f, float dampening = 0f, float friction = 0f)
        {
            this.target = target;
            Offset = offset;
            Velocity = velocity;
            Dampening = dampening;
            Friction = friction;
        }

        public void Update()
        {
            Update(Time.smoothDeltaTime);
        }

        public void Update(float deltaTime)
        {
            var mod = 1f + Dampening;
            var nextVelocity = (Velocity * math.exp((1f + mod * 5f) * -deltaTime)) - Offset * mod;
            nextVelocity -= nextVelocity * Friction * deltaTime;
            Offset += Velocity * deltaTime;
            Velocity = nextVelocity;
        }

        public void AddForce(float force, ForceMode2D forceMode)
        {
            switch (forceMode)
            {
                case ForceMode2D.Force:
                    Velocity += force * Time.smoothDeltaTime;
                    break;
                case ForceMode2D.Impulse:
                    Velocity += force;
                    break;
            }
        }

        public static implicit operator float(SpringFloat springFloat) => springFloat.Value;
    }
}
