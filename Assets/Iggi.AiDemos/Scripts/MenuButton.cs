﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Demo
{
    [RequireComponent(typeof(Button))]
    public class MenuButton : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
    {
        [SerializeField]
        private Text descriptionTextField = default;

        [SerializeField, Tooltip("Either a scene name, or a web link")]
        private string link = default;

        [SerializeField, TextArea]
        private string description = default;

        private float hoverEffectTarget = 0f;
        private float hoverEffectModifier = 0f;
        private float hoverEffectVelocity = 0f;

        private void Awake()
        {
            if (string.IsNullOrEmpty(link))
            {
#if UNITY_STANDALONE
                GetComponent<Button>().onClick.AddListener(() => Application.Quit());
#else
                Destroy(gameObject);
#endif
            }
            else
            {
                GetComponent<Button>().onClick.AddListener(() =>
                {
                    if(link.StartsWith("http"))
                    {
                        Application.OpenURL(link);
                    }
                    else
                    {
                        SceneManager.LoadScene(link);
                    }
                });
            }
        }

        private void Update()
        {
            hoverEffectModifier = Mathf.SmoothDamp(hoverEffectModifier, hoverEffectTarget, ref hoverEffectVelocity, Time.smoothDeltaTime);
            transform.localScale = Vector3.one * (1f + 0.08f * hoverEffectModifier);
        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            descriptionTextField.text = description;
            hoverEffectTarget = 1f;
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            descriptionTextField.text = string.Empty;
            hoverEffectTarget = 0f;
        }
    }
}
