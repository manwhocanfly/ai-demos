﻿using UnityEngine;
using System.Collections;

public static class GameObjectExtensions
{
    public static T GetOrAddComponent<T>(this GameObject gameObject) where T : Component
    {
        var component = gameObject.GetComponent<T>();
        return component ? component : gameObject.AddComponent<T>();
    }

    public static float GetAngle(this Transform transform)
    {
        return transform.localEulerAngles.z;
    }

    public static void SetAngle(this Transform transform, float angle)
    {
        transform.localEulerAngles = new Vector3(0f, 0f, angle);
    }
}
