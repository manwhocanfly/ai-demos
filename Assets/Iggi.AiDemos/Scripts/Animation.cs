﻿using System;
using System.Collections;
using UnityEngine;

namespace Iggi.AiDemos
{
    public struct Animation : IEnumerator
    {
        public delegate void AnimationCallback(float t);

        private float started;
        private readonly float duration;
        private readonly bool unscaledTime;
        private readonly AnimationCallback callback;
        private readonly Func<float, float> easing;

        private float progress
        {
            get
            {
                if (duration <= 0f) return 1f;
                return Mathf.Clamp01(((unscaledTime ? Time.unscaledTime : Time.time) - started) / duration);
            }
        }

        public bool finished => progress == 1f;
        public bool isPlaying => progress < 1f;
        public object Current => null;

        public Animation(float duration, Func<float, float> easing = null, bool unscaledTime = false, AnimationCallback callback = null)
        {
            this.started = unscaledTime ? Time.unscaledTime : Time.time;
            this.duration = duration;
            this.unscaledTime = unscaledTime;
            this.easing = easing;
            this.callback = callback;

            callback?.Invoke(progress);
        }

        public bool MoveNext()
        {
            callback?.Invoke(easing == null ? progress : easing(progress));
            return progress < 1f;
        }

        public void Reset()
        {
            started = unscaledTime ? Time.unscaledTime : Time.time;
        }
    }

}
