﻿using UnityEngine;
using System.Collections;
using Unity.Mathematics;

namespace Iggi.AiDemos.Yeetbot
{
    /// <summary>
    /// Input controls for Demo 1
    /// 
    /// - [Escape] Go back to previous screen
    /// - [Left Mouse Button] Spawn Red Box
    /// - [Right Mouse Button] Spawn Blue Box
    /// - [Middle Mouse Button] Destroy Box
    /// </summary>
    public class InputSystem
    {
        public void Update(World world)
        {
            // Escape - Go Back
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                world.GoBackToMenu();
            }

            // Left Mouse Button - Spawn Red Box
            if (Input.GetMouseButtonDown(0))
            {
                world.SpawnBox(world.Camera.ScreenToWorldPoint(Input.mousePosition), BoxType.Red);
            }

            // Right Mouse Button - Spawn Blue Box
            if (Input.GetMouseButtonDown(1))
            {
                world.SpawnBox(world.Camera.ScreenToWorldPoint(Input.mousePosition), BoxType.Blue);
            }

            // Middle Mouse Button - Destroy Box
            if (Input.GetMouseButton(2))
            {
                world.DestroyBoxAtPosition(world.Camera.ScreenToWorldPoint(Input.mousePosition));
            }
        }
    }
}
