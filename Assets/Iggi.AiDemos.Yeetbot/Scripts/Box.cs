﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Iggi.AiDemos.Yeetbot
{
    public enum BoxType
    {
        Red,
        Blue,
    }

    public class Box : MonoBehaviour
    {
        public delegate void CollisionCallback(Box box, Collision2D collision);

        #region Unity Inspector Properties

        [SerializeField] private BoxType type = default;
        [SerializeField] private Rigidbody2D physicsBody = default;
        [SerializeField] private SpriteRenderer targetRenderer = default;
        [SerializeField] private VFX vfxDestroyPrefab = default;
        [SerializeField] private TargetJoint2D joint = default;

        #endregion

        public static event CollisionCallback OnCollision;

        public BoxType Type => type;
        public Rigidbody2D PhysicsBody => physicsBody;
        public TargetJoint2D Joint => joint;

        public bool IsFixed
        {
            get { return physicsBody.isKinematic; }
            set
            {
                if (physicsBody)
                {
                    physicsBody.isKinematic = value;

                    if (value)
                    {
                        physicsBody.velocity = default;
                        physicsBody.angularVelocity = default;
                    }
                }
            }
        }

        public void SetType(BoxType type)
        {
            this.type = type;

            targetRenderer.color = type.GetColor();
        }

        public void Die()
        {
            VFX.Create(vfxDestroyPrefab, transform.position + Vector3.back * 7f, color: type.GetColor());

            Destroy(gameObject);
        }

        private void OnValidate()
        {
            SetType(type);
        }

        private void OnCollisionEnter2D(Collision2D collision)
        {
            OnCollision?.Invoke(this, collision);
        }
    }

    public static class BoxTypeExtensions
    {
        public static Color GetColor(this BoxType type)
        {
            switch (type)
            {
                case BoxType.Red:
                    return new Color(0.8f, 0.1f, 0.1f);

                case BoxType.Blue:
                    return new Color(0f, 0.5f, 1f);

                default:
                    return new Color(0.8f, 0.8f, 0.8f);
            }
        }
    }
}

