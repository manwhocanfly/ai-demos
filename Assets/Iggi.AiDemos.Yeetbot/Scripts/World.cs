﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using RNG = UnityEngine.Random;
using System;
using System.Linq;

namespace Iggi.AiDemos.Yeetbot
{
    public class World : StateMachine
    {
        #region Unity Inspector Properties

        [Header("UI")]
        [SerializeField] private Button buttonBack = null;
        [SerializeField] private RectTransform resourcesWindow = null;
        [SerializeField] private Text uiTextBoxCount = null;

        [Header("Game Objects")]
        [SerializeField] private Box boxPrefab = default;
        [SerializeField] private List<BoxContainer> boxContainers = new List<BoxContainer>();
        [SerializeField] private Camera worldCamera = null;
        [SerializeField] private int boxCount = 16;

        #endregion

        private InputSystem inputSystem = new InputSystem();
        private List<Box> boxes = new List<Box>();
        private SpringFloat uiRewardWindowBounceModifier = new SpringFloat(dampening: 2f);
        private SpringFloat cameraShakeModifierX = new SpringFloat(dampening: 5f);
        private SpringFloat cameraShakeModifierY = new SpringFloat(dampening: 5f);

        public Camera Camera => worldCamera;
        public IReadOnlyList<Box> Boxes => boxes;
        public IEnumerable<Box> AvailableBoxes => boxes.Where(x => x != null && x.IsFixed == false && x.transform.position.y < 2f);

        private void Awake()
        {
            Application.targetFrameRate = 60;

            Box.OnCollision += OnBoxCollision;
            buttonBack.onClick.AddListener(GoBackToMenu);
            uiTextBoxCount.text = boxCount.ToString();
        }

        private IEnumerator Start()
        {
            yield return new WaitForSeconds(0.1f);

            while (true)
            {
                for (int i = 0; i < 6; i++)
                {
                    yield return new WaitForSeconds((8 - i) * 0.05f);
                    SpawnRandomBox();
                }

                float timeout = default;

                while(true)
                {
                    timeout = AvailableBoxes.Count() > 0f ? 5f : (timeout - Time.smoothDeltaTime);
                    if (timeout < 0f) break;
                    yield return null;
                }
            }
        }

        protected override void Update()
        {
            base.Update();

            inputSystem.Update(this);

            UpdateUI();
            UpdateCamera();
        }

        private void UpdateUI()
        {
            uiRewardWindowBounceModifier.Update();
            resourcesWindow.localScale = Vector3.one * (1f + uiRewardWindowBounceModifier.Value);
            uiTextBoxCount.transform.localScale = Vector3.one * (1f + uiRewardWindowBounceModifier.Value * 2f);
        }

        private void UpdateCamera()
        {
            cameraShakeModifierX.Update();
            cameraShakeModifierY.Update();

            worldCamera.transform.position = new Vector3(cameraShakeModifierX.Value, 3.7f + cameraShakeModifierY.Value, -20f);
        }

        public void GoBackToMenu()
        {
            UnityEngine.SceneManagement.SceneManager.LoadScene(0);
        }

        public void ShakeCamera(Vector2 force)
        {
            cameraShakeModifierX.AddForce(force.x, ForceMode2D.Impulse);
            cameraShakeModifierY.AddForce(force.y, ForceMode2D.Impulse);
        }

        public BoxContainer GetBoxContainerOfType(BoxType boxType)
        {
            return boxContainers.Where(x => x.BoxType == boxType).FirstOrDefault();
        }

        public void DestroyBoxAtPosition(Vector2 worldPosition, bool spawnReward = false)
        {
            var collider = Physics2D.OverlapPoint(worldPosition);
            var box = collider ? collider.GetComponent<Box>() : default;

            if (box) DestroyBox(box, spawnReward);
        }

        public void DestroyBox(Box box, bool spawnReward = false)
        {
            if (!box) return;

            box.Die();

            if(spawnReward)
            {
                ShakeCamera(RNG.onUnitSphere * 1f);
            }

            boxes.Remove(box);

            boxCount++;
            uiTextBoxCount.text = boxCount.ToString();

            uiRewardWindowBounceModifier.AddForce(2f, ForceMode2D.Impulse);
        }

        public void SpawnRandomBox()
        {
            var side = RNG.value > 0.5f ? -1f : 1f;
            var type = RNG.value > 0.5f ? BoxType.Red : BoxType.Blue;
            var spawnPosition = new Vector3(RNG.Range(2f, 4f) * side, 0f, 0f);

            SpawnBox(spawnPosition, type);
        }

        public void SpawnBox(Vector2 worldPosition, BoxType type)
        {
            if (boxCount == 0) return;

            boxCount--;
            uiTextBoxCount.text = boxCount.ToString();
            uiRewardWindowBounceModifier.AddForce(-2f, ForceMode2D.Impulse);

            var spawnPosition = new Vector2(worldPosition.x, 11f);
            var box = Instantiate(boxPrefab, spawnPosition, Quaternion.identity);

            box.SetType(type);

            box.PhysicsBody.AddForce(Vector2.right * RNG.Range(-1f, 1f) * 0.1f, ForceMode2D.Impulse);
            box.PhysicsBody.AddTorque(RNG.Range(3f, 6f) * (RNG.value > 0.5f ? 1f : -1f), ForceMode2D.Impulse);

            boxes.Add(box);
        }

        private void OnBoxCollision(Box box, Collision2D collision)
        {
            if(collision.relativeVelocity.magnitude > 10f)
            {
                ShakeCamera(collision.relativeVelocity * 0.02f);
            }
        }
    }
}
