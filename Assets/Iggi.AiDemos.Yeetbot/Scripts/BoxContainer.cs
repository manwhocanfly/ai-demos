﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Iggi.AiDemos.Yeetbot
{
    public class BoxContainer : MonoBehaviour
    {
        #region Unity Inspector Properties

        [SerializeField] private BoxType boxType = default;
        [SerializeField] private VFX vfxRecycleBoxPrefab = default;
        [SerializeField] private ChainTrack chainTrack = default;
        [SerializeField] private float chainTrackSpeed = 1f;
        [SerializeField] private Facing facing = default;

        #endregion

        private Queue<Box> boxQueue = new Queue<Box>();
        public SpringFloat movementVelocity = new SpringFloat(dampening: 0f, friction: 4f);

        public World World { get; private set; }
        public BoxType BoxType => boxType;

        protected void OnEnable()
        {
            World = GetComponentInParent<World>();
            StartCoroutine(BoxQueueCoroutine());
        }

        protected void Update()
        {
            movementVelocity.Update();
            chainTrack.Speed = -movementVelocity.Value * chainTrackSpeed;
        }

        private void OnTriggerEnter2D(Collider2D collider)
        {
            var box = collider.GetComponent<Box>();

            if (!box) return;

            // Check if the box is the correct type to be recycled
            if(box.Type == BoxType)
            {
                World.DestroyBox(box, true);
                VFX.Create(vfxRecycleBoxPrefab, transform.position + Vector3.forward * 5f, 1f, boxType.GetColor());
            }
            else
            {
                box.IsFixed = true;
                boxQueue.Enqueue(box);
            }
        }
        
        private IEnumerator BoxQueueCoroutine()
        {
            while(true)
            {
                yield return null;

                if (boxQueue.Count == 0) continue;

                movementVelocity.Target = facing.GetFloat(1f);

                yield return StartCoroutine(DisposeBox(boxQueue.Dequeue()));

                movementVelocity.Target = 0f;
            }
        }

        private IEnumerator DisposeBox(Box box)
        {
            var boxPosition = box.transform.position;
            var startPosition = chainTrack.transform.position + new Vector3(facing.GetFloat(-0.5f), 0.48f, 0f);
            var endPosition = chainTrack.transform.position + new Vector3(facing.GetFloat(0.5f), 0.48f, 0f);

            var startAngle = box.transform.GetAngle();

            yield return new Animation(0.2f, callback: (t) => 
            {
                if (box == false) return;

                box.transform.position = Vector2.Lerp(boxPosition, startPosition, t);
                box.transform.SetAngle(Mathf.LerpAngle(startAngle, 0, t));
            });

            while (box)
            {
                box.transform.position += facing.GetVector(Vector3.right) * Time.smoothDeltaTime * 2f;

                if (facing.GetFloat(box.transform.position.x - endPosition.x) > 0f) break;

                yield return null;
            }

            box.IsFixed = false;

            yield return new Animation(0.2f, callback: (t) =>
            {
                if (box == false) return;

                box.PhysicsBody.AddForce(facing.GetVector(Vector2.right) * 20f, ForceMode2D.Force);
            });
        }
    }
}

