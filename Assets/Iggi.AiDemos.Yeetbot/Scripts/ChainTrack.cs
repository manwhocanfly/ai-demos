﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Iggi.AiDemos.Yeetbot
{
    public class ChainTrack : MonoBehaviour
    {
        private static readonly int SHADER_MAINTEX_ID = Shader.PropertyToID("_MainTex");

        #region Unity Inspector Properties

        [SerializeField] private LineRenderer lineRenderer = default;
        [SerializeField] private Transform[] wheels = default;
        [SerializeField] private Transform[] wheelBackgrounds = default;
        [SerializeField] private SpriteRenderer background = default;

        [SerializeField] private float trackSpeed = 0f;
        [SerializeField] private float trackMoved = 0f;

        [SerializeField] private bool inverted = default;
        [SerializeField] private float radiusLeft = 1f;
        [SerializeField] private float radiusRight = 1f;
        [SerializeField] private float length = 1f;
        [SerializeField] private int points = 8;

        #endregion

        private Vector3[] pointPositions = new Vector3[0];

        public float Speed { get => trackSpeed; set => trackSpeed = value; }
        public float Distance { get => trackMoved; set => trackMoved = value; }

        private void Update()
        {
            trackMoved += trackSpeed * Time.smoothDeltaTime;
            lineRenderer.material.SetTextureOffset(SHADER_MAINTEX_ID, new Vector2(trackMoved, 0f));
        }

        public void SetWheelProperties(float? radiusLeft = default, float? radiusRight = default, float? length = default)
        {
            var changed = false;

            if (radiusLeft.HasValue && Mathf.Approximately(radiusLeft.Value, this.radiusLeft) == false)
            {
                this.radiusLeft = radiusLeft.Value;
                changed = true;
            }

            if (radiusRight.HasValue && Mathf.Approximately(radiusRight.Value, this.radiusRight) == false)
            {
                this.radiusRight = radiusRight.Value;
                changed = true;
            }

            if (length.HasValue && Mathf.Approximately(length.Value, this.length) == false)
            {
                this.length = length.Value;
                changed = true;
            }

            if (changed)
            {
                RebuildPoints();
            }
        }

        [ContextMenu("Rebuild Points")]
        private void RebuildPoints()
        {
            var pointCount = points * 2;

            if (pointPositions.Length != pointCount)
            {
                Array.Resize(ref pointPositions, pointCount);
            }

            if (lineRenderer.positionCount != pointCount)
            {
                lineRenderer.positionCount = pointCount;
            }

            var lrDiff = radiusLeft - radiusRight;
            var rlDiff = radiusRight - radiusLeft;
            var angleDiff = Mathf.PI / (points - 1);
            var lengthOffset = new Vector3(length / 2f, 0f, 0f);

            for (int i = 0; i < points; i++)
            {
                var angle = i * angleDiff + Mathf.PI;
                var point = new Vector3(Mathf.Sin(angle), Mathf.Cos(angle) + 1f) * radiusLeft;

                if (rlDiff > 0f && i == 7) point.y += rlDiff * 0.3f;
                if (rlDiff > 0f && i == 6) point.y += rlDiff * 0.1f;

                var id = inverted ? (pointCount - 1 - i) : i;

                pointPositions[id] = point - lengthOffset;
            }

            for (int i = 0; i < points; i++)
            {
                var angle = i * angleDiff;
                var point = new Vector3(Mathf.Sin(angle), Mathf.Cos(angle) + 1f) * radiusRight;

                if (lrDiff > 0f && i == 0) point.y += lrDiff * 0.3f;
                if (lrDiff > 0f && i == 1) point.y += lrDiff * 0.1f;

                var id = inverted ? (points - 1 - i) : (i + points);

                pointPositions[id] = point + lengthOffset;
            }

            lineRenderer.SetPositions(pointPositions);

            // Wheels
            var wheelLeft = new Vector3(-length / 2f, radiusLeft, 0f);
            var wheelRight = new Vector3(length / 2f, radiusRight, 0f);

            for (int i = 0; i < wheels.Length; i++)
            {
                var wheel = wheels[i];
                if (wheel == false) continue;

                var t = wheels.Length == 1 ? 0.5f : (i / (float)(wheels.Length - 1));

                wheel.transform.position = transform.position + Vector3.Lerp(wheelLeft, wheelRight, t) + Vector3.back * 2f;
            }

            for (int i = 0; i < wheelBackgrounds.Length; i++)
            {
                var wheel = wheelBackgrounds[i];
                if (wheel == false) continue;

                var t = wheels.Length == 1 ? 0.5f : (i / (float)(wheels.Length - 1));

                wheel.transform.position = transform.position + Vector3.Lerp(wheelLeft, wheelRight, t) + Vector3.back * 1f;
                wheel.localScale = Vector3.one * Mathf.Lerp(radiusLeft, radiusRight, t) * 3f;
            }

            background.size = new Vector2(length + radiusLeft + radiusRight, 0.13f);
            background.transform.position = transform.position + Vector3.Lerp(wheelLeft, wheelRight, 0.5f) + Vector3.back * 1f;
            background.transform.SetAngle(Vector2.Angle(Vector2.right, wheelLeft - wheelRight));
        }
    }
}
