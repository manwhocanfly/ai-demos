﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Unity.Mathematics;
using UnityEngine;

namespace Iggi.AiDemos.Yeetbot
{
    public class Yeetbot : StateMachine
    {
        #region Unity Inspector Properties

        [Header("Game Objects")]
        [SerializeField] private Rigidbody2D physicsBody = default;
        [SerializeField] private YeetbotAvatar avatar = default;
        [SerializeField] private YeetbotScanner scanner = default;
        [SerializeField] private float movementSpeed = 8f;

        #endregion

        public override State DefaultState => gameObject.GetOrAddComponent<YeetbotSleepState>();

        private float handGripStrenght = 0.15f;
        private float handGrabDistance = 0.05f;
        private float handThrowDistance = 3.2f;

        public World World { get; private set; }
        public Rigidbody2D PhysicsBody => physicsBody;
        public YeetbotAvatar Avatar => avatar;
        public YeetbotScanner Scanner => scanner;
        public Facing Facing { get; set; } = Facing.Right;
        public Box CarryingBox { get; private set; } = default;
        public bool IsCarryingBox => CarryingBox;

        public IEnumerable<Box> BoxesInSight => World.AvailableBoxes.Where(x => IsFacing(x));
        public Box ClosestBox => World.AvailableBoxes.OrderBy(x => Vector2.Distance(x.transform.position, avatar.upperArm.position)).FirstOrDefault();
        public Box ClosestBoxInSight => BoxesInSight.OrderBy(x => Vector2.Distance(x.transform.position, avatar.upperArm.position)).FirstOrDefault();

        /// <summary>
        /// Find out if the object is close enough to be picked up
        /// </summary>
        public bool IsAtArmsLength(Component component)
        {
            if (!component) return false;
            var distance = Vector2.Distance(avatar.upperArm.position, component.transform.position);
            return distance < (avatar.ArmLength - handGrabDistance);
        }

        /// <summary>
        /// Find out if the object is close enough to be grabbed
        /// </summary>
        public bool IsAtGrabLength(Component component)
        {
            if (!component) return false;
            var distance = Vector2.Distance(avatar.hand.position, component.transform.position);
            return distance < handGrabDistance;
        }

        /// <summary>
        /// Is the object close enough that you could hit it with a box throw
        /// </summary>
        public bool IsAtThrowingDistance(Component component)
        {
            if (!component) return false;
            var distance = Vector2.Distance(avatar.upperArm.position, component.transform.position);
            return distance < handThrowDistance; // TODO make smarter way to calculate throw distance
        }

        /// <summary>
        /// Find out if the Yeetbot is facing object
        /// </summary>
        public bool IsFacing(Component component)
        {
            if (component == false) return false;
            var diff = component.transform.position - transform.position;
            return Mathf.Approximately(Facing.GetSign(), Mathf.Sign(diff.x));
        }

        public float DistanceTo(Component component)
        {
            if (!component) return float.PositiveInfinity;
            return Vector2.Distance(avatar.upperArm.position, component.transform.position);
        }

        protected void Awake()
        {
#if UNITY_EDITOR
            OnStateChange += () => Debug.Log($"[{Time.time}] {(CurrentState ? CurrentState.GetType().Name : "-")}");
#endif
        }

        protected override void OnEnable()
        {
            base.OnEnable();

            World = GetComponentInParent<World>();
        }

        protected override void Update()
        {
            base.Update();
        }

        protected void FixedUpdate()
        {
            UpdateCarryingBox();
            UpdateMovement();
        }

        private void UpdateMovement()
        {
            physicsBody.MovePosition(physicsBody.position + Vector2.right * Avatar.movementVelocity * movementSpeed * Time.fixedDeltaTime);
        }

        private void UpdateCarryingBox()
        {
            if (CarryingBox == false) return;

            if (CarryingBox.IsFixed)
            {
                CarryingBox.transform.position = Vector2.Lerp(CarryingBox.transform.position, avatar.hand.position, 0.3f);
            }
            else
            {
                var targetPosition = Vector2.Lerp(CarryingBox.transform.position, avatar.hand.position, 0.1f);
                CarryingBox.PhysicsBody.MovePosition(targetPosition);

                CarryingBox.Joint.target = avatar.hand.position;
                CarryingBox.PhysicsBody.angularVelocity *= 0.8f;

                // Check if we lost it, for example when some other box pushes it away
                var distanceToBoxJoint = Vector2.Distance(CarryingBox.transform.position, avatar.hand.position);

                if (handGripStrenght < 0f || distanceToBoxJoint > (handGrabDistance * 4f))
                {
                    ReleaseBox();
                }
                else if (distanceToBoxJoint > handGrabDistance * 2f)
                {
                    handGripStrenght -= Time.fixedDeltaTime;
                }
                else if (distanceToBoxJoint > handGrabDistance)
                {
                    handGripStrenght -= Time.fixedDeltaTime * 0.2f;
                }
                else
                {
                    handGripStrenght = 0.15f;
                }
            }
        }

        public bool TryGrabBox(Box box)
        {
            if (box == false || CarryingBox || IsAtGrabLength(box) == false || box.IsFixed) return false;

            handGripStrenght = 0.15f;
            box.Joint.target = avatar.hand.position;
            box.Joint.enabled = true;

            CarryingBox = box;

            return true;
        }

        public void ReleaseBox()
        {
            if (!CarryingBox) return;

            CarryingBox.Joint.enabled = false;
            CarryingBox = null;
        }
    }

    public abstract class YeetbotState : StateMachine.State
    {
        public Yeetbot Yeetbot => (Yeetbot)StateMachine;
        public YeetbotAvatar Avatar => Yeetbot.Avatar;
    }
}

