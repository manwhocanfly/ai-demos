﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Iggi.AiDemos.Yeetbot
{
    public class YeetbotScanner : MonoBehaviour
    {
        private Yeetbot yeetbot;
        private Collider2D[] sensorResult;
        private int sensorCount;

        protected void Awake()
        {
            yeetbot = GetComponent<Yeetbot>();
            sensorResult = new Collider2D[50];
        }

        public IEnumerable<Collider2D> Scan(float radius)
        {
            return Scan(yeetbot.Avatar.lowerBody.position, radius);
        }

        public IEnumerable<Collider2D> Scan(Vector2 position, float radius)
        {
            sensorCount = Physics2D.OverlapCircleNonAlloc(position, radius, sensorResult);

            for (int i = 0; i < sensorCount; i++)
            {
                yield return sensorResult[i];
            }
        }

    }
}
