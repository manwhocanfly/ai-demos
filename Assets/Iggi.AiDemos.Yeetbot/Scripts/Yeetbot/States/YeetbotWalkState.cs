﻿using UnityEngine;
using System.Collections;
using System.Linq;

namespace Iggi.AiDemos.Yeetbot
{
    /// <summary>
    /// Walk until yeetbot hits an obstacle
    /// </summary>
    public class YeetbotWalkState : YeetbotState
    {
        private readonly float searchRadius = 4f;
        private float attentionSpan;

        public override void OnStateStart()
        {
            attentionSpan = 0.4f;
            Yeetbot.Avatar.movementVelocity.Target = Yeetbot.Facing.GetFloat(1f);
        }

        public override void OnStateEnd()
        {
            Yeetbot.Avatar.movementVelocity.Target = 0f;
        }

        public override void OnStateUpdate()
        {
            if (attentionSpan < 0f)
            {
                Yeetbot.GotoState<YeetbotIdleState>();
            }
            else if (Yeetbot.IsCarryingBox == false && Yeetbot.BoxesInSight.Count() == 0)
            {
                attentionSpan -= Time.smoothDeltaTime;
            }

            if (ScanForObstacles())
            {
                if (Yeetbot.IsCarryingBox)
                {
                    Yeetbot.GotoState<YeetbotThrowForwardsState>();
                }
                else
                {
                    Yeetbot.GotoState<YeetbotIdleState>();
                }
            }
            else if (Timer > 0.2f)
            {
                Yeetbot.Avatar.ResetPose();
            }
        }

        private bool ScanForObstacles()
        {
            foreach (var collider in Yeetbot.Scanner.Scan(searchRadius))
            {
                if (collider == false || collider.attachedRigidbody == false) continue;
                if (Yeetbot.IsFacing(collider) == false) continue;

                var box = collider.attachedRigidbody.GetComponent<Box>();
                var boxContainer = collider.attachedRigidbody.GetComponent<BoxContainer>();

                if (boxContainer)
                {
                    if (Yeetbot.IsCarryingBox)
                    {
                        if (Yeetbot.IsAtThrowingDistance(boxContainer) == false) continue;
                    }
                    else
                    {
                        if (Yeetbot.DistanceTo(collider) > 1.3f) continue;

                        var direction = Yeetbot.IsFacing(boxContainer) ? 1f : -1f;

                        Yeetbot.Avatar.movementVelocity.Velocity = 0f;
                        Yeetbot.Avatar.movementVelocity.AddForce(Yeetbot.Facing.GetFloat(-10f) * direction, ForceMode2D.Impulse);
                    }

                    return true;
                }

                if (box)
                {
                    if (Yeetbot.IsCarryingBox)
                    {
                        if (box == Yeetbot.CarryingBox) continue;
                        if (Yeetbot.DistanceTo(box) > Yeetbot.Avatar.ArmLength * 2f) continue;
                    }
                    else
                    {
                        if (Yeetbot.IsAtArmsLength(box) == false) continue;
                    }

                    return true;
                }
            }

            return false;
        }
    }
}