﻿using UnityEngine;
using System.Collections;

namespace Iggi.AiDemos.Yeetbot
{
    public class YeetbotTurnAroundState : YeetbotState
    {
        public override void OnStateStart() => StartCoroutine(StateCoroutine());

        private IEnumerator StateCoroutine()
        {
            Yeetbot.Facing = Yeetbot.Facing.GetOpposite();

            var avatar = Yeetbot.Avatar;
            var facing = Yeetbot.Facing;
            var timeScale = Yeetbot.IsCarryingBox ? 1f : 0.5f;

            var armAngleFrom = avatar.armSpring.Target;
            var armFlexFrom = avatar.armFlex.Target;

            if (Yeetbot.IsCarryingBox)
            {
                avatar.bodySpring.AddForce(facing.GetFloat(80f), ForceMode2D.Impulse);
            }

            avatar.eyePosition.Target = facing.GetFloat(1f);
            avatar.armSpring.Dampening = 5f;
            avatar.ResetChainTrack();

            yield return new Animation(1f * timeScale, callback: (t) =>
            {
                avatar.armSpring.Target = Mathf.LerpUnclamped(armAngleFrom, avatar.IdlePoseArmAngle, t);
                avatar.armFlex.Target = Mathf.LerpUnclamped(armFlexFrom, avatar.IdlePoseArmFlex, Easing.Pow2.Out(t));
            });

            avatar.eye.localScale = facing.GetVector(Vector3.one);

            yield return new Animation(0.3f, callback: (t) =>
            {
                avatar.armSpring.Offset -= avatar.armSpring.Offset * Time.smoothDeltaTime;
            });

            avatar.armSpring.Dampening = 0f;

            Yeetbot.GotoState<YeetbotIdleState>();
        }
    }
}