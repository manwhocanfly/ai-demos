﻿using UnityEngine;
using System.Collections;
using System;
using System.Linq;

namespace Iggi.AiDemos.Yeetbot
{
    public class YeetbotSleepState : YeetbotState
    {
        private const float MIN_SCALE = 0.01f;

        public override void OnStateStart() => StartCoroutine(StateCoroutine());

        private IEnumerator StateCoroutine()
        {
            if(Yeetbot.PreviousState == null)
            {
                ApplySleepPose();
            }
            else
            {
                yield return StartCoroutine(Disassemble());
            }

            yield return null;

            while (Yeetbot.World.AvailableBoxes.Count() == 0) yield return null;

            if(Yeetbot.IsFacing(Yeetbot.ClosestBox) == false)
            {
                Yeetbot.Facing = Yeetbot.Facing.GetOpposite();
            }

            yield return new WaitForSeconds(Yeetbot.PreviousState == null ? 3f : 1f);

            yield return StartCoroutine(Assemble());

            Yeetbot.GotoState<YeetbotIdleState>();
        }

        private void ApplySleepPose()
        {
            Avatar.chainTrackLeftRadius.Value = Avatar.SleepPoseSmallWheelRadius;
            Avatar.chainTrackRightRadius.Value = Avatar.SleepPoseBigWheelRadius;
            Avatar.chainTrackLength.Value = Avatar.SleepPoseChainTrackLength;
            Avatar.lowerBody.localPosition = Vector3.up * Avatar.SleepPoseLowerBodyOffset;
            Avatar.middleBody.localPosition = Vector3.up * Avatar.SleepPoseMiddleBodyOffset;
            Avatar.upperBody.localPosition = Vector3.up * Avatar.SleepPoseUpperBodyOffset;
            Avatar.armSpring.Value = Avatar.SleepPoseArmAngle;
            Avatar.armFlex.Value = Avatar.SleepPoseArmFlex;
            Avatar.upperArm.localScale = Vector3.one * MIN_SCALE;
            Avatar.upperArmMuscle.localScale = Vector3.one * MIN_SCALE;
            Avatar.lowerArmMuscle.localScale = Vector3.one * MIN_SCALE;
            Avatar.hand.localScale = Vector3.one * MIN_SCALE;
            Avatar.lowerArm.localPosition = Vector3.right * Avatar.SleepPoseArmOffset + Vector3.forward;
        }

        private IEnumerator Disassemble()
        {
            var phaseInterval = 0.2f;

            Avatar.eyePosition.Target = Yeetbot.Facing.GetFloat(0f);
            Avatar.armFlex.Target = Avatar.SleepPoseArmFlex;

            yield return new Animation(phaseInterval, callback: (t) =>
            {
                Avatar.lowerArmMuscle.localScale = Vector3.one * Mathf.LerpUnclamped(1f, MIN_SCALE, t);
                Avatar.hand.localScale = Vector3.one * Mathf.LerpUnclamped(1f, MIN_SCALE, t);
            });

            Avatar.armSpring.Target = Avatar.SleepPoseArmAngle;

            yield return new Animation(phaseInterval, Easing.Back.Out, callback: (t) =>
            {
                Avatar.upperArmMuscle.localScale = Vector3.one * Mathf.LerpUnclamped(1f, MIN_SCALE, t);
                Avatar.lowerArm.localPosition = Vector3.right * Mathf.LerpUnclamped(Avatar.IdlePoseArmOffset, Avatar.SleepPoseArmOffset, t);
            });

            Avatar.chainTrackLeftRadius.Target = Avatar.SleepPoseSmallWheelRadius;
            Avatar.chainTrackRightRadius.Target = Avatar.SleepPoseSmallWheelRadius;
            Avatar.chainTrackLength.Target = Avatar.SleepPoseChainTrackLength;

            yield return new Animation(phaseInterval, callback: (t) =>
            {
                Avatar.upperBody.localPosition = Vector3.up * Mathf.LerpUnclamped(Avatar.IdlePoseUpperBodyOffset, Avatar.SleepPoseUpperBodyOffset, t);
            });

            yield return new Animation(phaseInterval, Easing.Back.Out, callback: (t) =>
            {
                Avatar.middleBody.localPosition = Vector3.up * Mathf.LerpUnclamped(Avatar.IdlePoseMiddleBodyOffset, Avatar.SleepPoseMiddleBodyOffset, t);
            });

            yield return new Animation(phaseInterval, Easing.Back.Out, callback: (t) =>
            {
                Avatar.lowerBody.localPosition = Vector3.up * Mathf.LerpUnclamped(Avatar.IdlePoseLowerBodyOffset, Avatar.SleepPoseLowerBodyOffset, t);
            });

            yield return new Animation(phaseInterval * 0.5f, callback: (t) =>
            {
                Avatar.upperArm.localScale = Vector3.one * Mathf.LerpUnclamped(1.3f, MIN_SCALE, t);
            });

            yield break;
        }

        private IEnumerator Assemble()
        {
            var phaseInterval = Yeetbot.PreviousState == null ? 0.35f : 0.25f;

            yield return new WaitForSeconds(0.4f);

            if (Yeetbot.PreviousState == null)
            {
                Avatar.eyePosition.Target = Yeetbot.Facing.GetFloat(1f);
                yield return new WaitForSeconds(1.2f);
                Avatar.eyePosition.Target = Yeetbot.Facing.GetFloat(0f);
                yield return new WaitForSeconds(0.4f);
                Avatar.eyePosition.Target = Yeetbot.Facing.GetFloat(-1f);
                yield return new WaitForSeconds(1.2f);
                Avatar.eyePosition.Target = Yeetbot.Facing.GetFloat(0f);
                yield return new WaitForSeconds(0.4f);
            }

            yield return new Animation(phaseInterval * 0.5f, Easing.Back.Out, callback: (t) =>
            {
                Avatar.upperArm.localScale = Vector3.one * Mathf.LerpUnclamped(MIN_SCALE, 1f, t);
            });

            Avatar.eyePosition.Target = Yeetbot.Facing.GetFloat(1f);

            yield return new WaitForSeconds(phaseInterval);

            yield return new Animation(phaseInterval, Easing.Back.Out, callback: (t) =>
            {
                Avatar.upperBody.localPosition = Vector3.up * Mathf.LerpUnclamped(Avatar.SleepPoseUpperBodyOffset, Avatar.IdlePoseUpperBodyOffset, t);
            });

            yield return new Animation(phaseInterval, Easing.Back.Out, callback: (t) =>
            {
                Avatar.middleBody.localPosition = Vector3.up * Mathf.LerpUnclamped(Avatar.SleepPoseMiddleBodyOffset, Avatar.IdlePoseMiddleBodyOffset, t);
            });

            yield return new Animation(phaseInterval, Easing.Back.Out, callback: (t) =>
            {
                Avatar.lowerBody.localPosition = Vector3.up * Mathf.LerpUnclamped(Avatar.SleepPoseLowerBodyOffset, Avatar.IdlePoseLowerBodyOffset, t);
            });

            yield return new Animation(phaseInterval, callback: (t) =>
            {
                Avatar.chainTrackLength.Target = Mathf.LerpUnclamped(Avatar.SleepPoseChainTrackLength, Avatar.IdlePoseChainTrackLength, t);
            });

            Avatar.ResetChainTrack();

            Avatar.armSpring.Value = Avatar.SleepPoseArmAngle;
            Avatar.armSpring.Target = Avatar.IdlePoseArmAngle;

            yield return new Animation(phaseInterval, Easing.Back.Out, callback: (t) =>
            {
                Avatar.upperArmMuscle.localScale = Vector3.one * Mathf.LerpUnclamped(MIN_SCALE, 1f, t);
                Avatar.lowerArm.localPosition = Vector3.right * Mathf.LerpUnclamped(Avatar.SleepPoseArmOffset, Avatar.IdlePoseArmOffset, t);
            });

            Avatar.armFlex.Target = Avatar.IdlePoseArmFlex;

            yield return new Animation(phaseInterval, Easing.Back.Out, callback: (t) =>
            {
                Avatar.lowerArmMuscle.localScale = Vector3.one * Mathf.LerpUnclamped(MIN_SCALE, 1f, t);
                Avatar.hand.localScale = Vector3.one * Mathf.LerpUnclamped(MIN_SCALE, 1.3f, t);
            });

            yield return new WaitForSeconds(0.1f);

            yield break;
        }
    }
}