﻿using UnityEngine;
using System.Collections;

namespace Iggi.AiDemos.Yeetbot
{
    public class YeetbotThrowBackwardsState : YeetbotState
    {
        private Vector2 throwForce = new Vector2(-10f, 20f);

        public override void OnStateStart() => StartCoroutine(StateCoroutine());

        private IEnumerator StateCoroutine()
        {
            if (Yeetbot.IsCarryingBox == false)
            {
                Yeetbot.GotoState<YeetbotIdleState>();
                yield break;
            }

            yield return Yeetbot.Avatar.StartCoroutine(ThrowBoxBackwards());

            if (Yeetbot.IsCarryingBox)
            {
                var force = Yeetbot.Facing.GetVector(throwForce);

                Yeetbot.CarryingBox.PhysicsBody.AddForce(force, ForceMode2D.Impulse);
                Yeetbot.ReleaseBox();
            }

            yield return new WaitForSeconds(0.7f);

            Yeetbot.GotoState<YeetbotIdleState>();
        }

        public IEnumerator ThrowBoxBackwards()
        {
            var facing = Yeetbot.Facing;
            var avatar = Yeetbot.Avatar;

            // Prepare for throw
            yield return new WaitUntil(() => {
                avatar.armSpring.Target = avatar.IdlePoseArmAngle;
                avatar.armFlex.Target = avatar.IdlePoseArmFlex;

                return MathUtil.CloseEnough(avatar.IdlePoseArmAngle, avatar.armSpring.Value, 1f);
            });

            // Check if box still in hands
            if (Yeetbot.IsCarryingBox == false)
            {
                avatar.ResetPose();
                yield break;
            }

            // Yeet
            avatar.bodySpring.AddForce(facing.GetFloat(-40f), ForceMode2D.Impulse);
            Yeetbot.CarryingBox.IsFixed = true;

            yield return new Animation(0.3f, Easing.Back.In, callback: (t) =>
            {
                avatar.armSpring.Value = avatar.IdlePoseArmAngle + facing.GetFloat(75f * t);

                if (Yeetbot.IsCarryingBox)
                {
                    Yeetbot.CarryingBox.transform.position = (Vector2)avatar.hand.position;
                }
            });

            avatar.armSpring.AddForce(facing.GetFloat(1200f), ForceMode2D.Impulse);
            Yeetbot.CarryingBox.IsFixed = false;
        }
    }
}