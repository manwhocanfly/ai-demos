﻿using UnityEngine;
using System.Collections;
using System.Linq;

namespace Iggi.AiDemos.Yeetbot
{
    public class YeetbotPickUpBoxState : YeetbotState
    {
        public override void OnStateStart() => StartCoroutine(StateCoroutine());

        private IEnumerator StateCoroutine()
        {
            while (true)
            {
                // Move arms in position to pick up the box
                var targetBox = Yeetbot.ClosestBoxInSight;

                if (Yeetbot.IsCarryingBox || targetBox == false || Yeetbot.IsAtArmsLength(targetBox) == false) break;

                if (Timer > 2f)
                {
                    Yeetbot.Avatar.movementVelocity.AddForce(Yeetbot.Facing.GetFloat(8f), ForceMode2D.Impulse);
                    break;
                }

                Yeetbot.Avatar.MoveHandTowardsPoint(targetBox.transform.position);

                // Try picking up the box
                if (Yeetbot.IsAtGrabLength(targetBox) && Yeetbot.TryGrabBox(targetBox))
                {
                    yield return new WaitForSeconds(0.1f);

                    Yeetbot.Avatar.movementVelocity.Target = Yeetbot.Facing.GetFloat(-1f);

                    yield return new WaitForSeconds(0.1f);

                    while (true)
                    {
                        // Steer clear of all obstacles
                        if (Yeetbot.IsCarryingBox == false) break;

                        Yeetbot.Avatar.armSpring.AddForce(Yeetbot.Facing.GetFloat(1000f), ForceMode2D.Force);

                        var scanPosition = (Vector2)Yeetbot.Avatar.lowerBody.position + Yeetbot.Facing.GetVector(new Vector2(0f, 0f));
                        var scanResult = Yeetbot.Scanner.Scan(Yeetbot.Avatar.lowerBody.position, 1.5f);

                        scanResult = scanResult.Where(x => x.attachedRigidbody != null);

                        if (scanResult.Select(x => x.attachedRigidbody.GetComponent<BoxContainer>())
                            .Where(x => x != null)
                            .Count() > 0)
                        {
                            break;
                        }

                        if (scanResult.Select(x => x.attachedRigidbody.GetComponent<Box>())
                            .Where(x => x != null)
                            .Where(x => Yeetbot.CarryingBox != x)
                            .Where(x => x.transform.position.y < Yeetbot.CarryingBox.transform.position.y)
                            .Where(x => Yeetbot.IsFacing(x))
                            .Count() > 0)
                        {
                            yield return null;
                            continue;
                        }

                        break;
                    }

                    // Get into to default carrying position
                    Yeetbot.Avatar.movementVelocity.Target = 0f;
                    Yeetbot.Avatar.movementVelocity.AddForce(Yeetbot.Facing.GetFloat(-8f), ForceMode2D.Impulse);

                    var armSpringFrom = Yeetbot.Avatar.armSpring.Target;
                    var armFlexFrom = Yeetbot.Avatar.armFlex.Target;

                    yield return new Animation(0.3f, callback: (t) =>
                    {
                        Yeetbot.Avatar.armSpring.Target = Mathf.Lerp(armSpringFrom, Yeetbot.Avatar.IdlePoseArmAngle, t);
                        Yeetbot.Avatar.armFlex.Target = Mathf.Lerp(armFlexFrom, Yeetbot.Avatar.IdlePoseArmFlex, t);
                    });

                    yield return new WaitForSeconds(0.1f);

                    break;
                }

                yield return null;
            }

            Yeetbot.GotoState<YeetbotIdleState>();
        }

    }
}