﻿using UnityEngine;
using System.Collections;

namespace Iggi.AiDemos.Yeetbot
{
    public class YeetbotThrowForwardsState : YeetbotState
    {
        private Vector2 throwForce = new Vector2(12f, 22f);

        public override void OnStateStart() => StartCoroutine(StateCoroutine());

        private IEnumerator StateCoroutine()
        {
            if (Yeetbot.IsCarryingBox == false)
            {
                Yeetbot.GotoState<YeetbotIdleState>();
                yield break;
            }

            yield return Yeetbot.Avatar.StartCoroutine(ThrowBoxForwards());

            if (Yeetbot.IsCarryingBox)
            {
                var force = Yeetbot.Facing.GetVector(throwForce);

                Yeetbot.CarryingBox.PhysicsBody.AddForce(force, ForceMode2D.Impulse);
                Yeetbot.ReleaseBox();
            }

            yield return new WaitForSeconds(0.7f);

            Yeetbot.GotoState<YeetbotIdleState>();
        }

        public IEnumerator ThrowBoxForwards()
        {
            var facing = Yeetbot.Facing;
            var avatar = Yeetbot.Avatar;

            // Prepare for throw
            var armAngleFrom = avatar.armSpring.Target;
            var armFlexFrom = avatar.armFlex.Target;

            var armAngleTo = facing.GetAngle(100f, 90f, -90f);
            var armFlexTo = facing.GetFloat(0.3f);

            avatar.bodySpring.Target = facing.GetFloat(5f);

            yield return new Animation(0.7f, callback: (t) =>
            {
                avatar.armSpring.Target = Mathf.LerpUnclamped(armAngleFrom, armAngleTo, t);
                avatar.armFlex.Target = Mathf.LerpUnclamped(armFlexFrom, armFlexTo, t);
            });

            yield return new WaitUntil(() => {
                avatar.armSpring.Target = armAngleTo;
                return MathUtil.CloseEnough(armAngleTo, avatar.armSpring.Value, 1f);
            });

            yield return new WaitForSeconds(0.1f);

            // Check if box still in hands
            if (Yeetbot.IsCarryingBox == false)
            {
                avatar.ResetPose();
                yield break;
            }

            // Yeet
            avatar.bodySpring.AddForce(facing.GetFloat(40f), ForceMode2D.Impulse);
            avatar.bodySpring.Target = 0f;

            Yeetbot.CarryingBox.IsFixed = true;

            armAngleFrom = avatar.armSpring.Value;
            armFlexFrom = avatar.armFlex.Value;

            avatar.armSpring.Velocity = 0f;
            avatar.armFlex.Velocity = 0f;

            yield return new Animation(0.2f, Easing.Back.In, callback: (t) =>
            {
                avatar.armSpring.Value = armAngleFrom - facing.GetFloat(45f * t);
                avatar.armFlex.Value = Mathf.LerpUnclamped(armFlexFrom, facing.GetFloat(0.1f), t);

                if (Yeetbot.IsCarryingBox)
                {
                    Yeetbot.CarryingBox.transform.position = (Vector2)avatar.hand.position;
                }
            });

            avatar.armSpring.AddForce(facing.GetFloat(-800f), ForceMode2D.Impulse);
            Yeetbot.CarryingBox.IsFixed = false;
        }
    }
}