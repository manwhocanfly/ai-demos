﻿using UnityEngine;
using System.Collections;
using System.Linq;

namespace Iggi.AiDemos.Yeetbot
{
    /// <summary>
    /// If carrying box, we either throw it or get closer to the box container
    /// If not carrying box, we scan for a good candidate and pick it up or start walking towards it
    /// </summary>
    public class YeetbotIdleState : YeetbotState
    {
        public override void OnStateStart() => StartCoroutine(StateCoroutine());
        public override void OnStateEnd() => StopAllCoroutines();

        private IEnumerator StateCoroutine()
        {
            while (true)
            {
                if (Timer > 0.2f) Yeetbot.Avatar.ResetPose();

                if (Timer > 2f && Yeetbot.World.AvailableBoxes.Count() == 0)
                {
                    Yeetbot.GotoState<YeetbotSleepState>();
                    yield break;
                }

                yield return null;

                if (Yeetbot.IsCarryingBox)
                {
                    var targetBoxContainer = Yeetbot.World.GetBoxContainerOfType(Yeetbot.CarryingBox.Type);

                    if (targetBoxContainer && Yeetbot.IsFacing(targetBoxContainer))
                    {
                        if (Yeetbot.IsFacing(targetBoxContainer) || Yeetbot.IsAtThrowingDistance(targetBoxContainer) == false)
                        {
                            Yeetbot.GotoState<YeetbotWalkState>();
                        }
                        else if (Yeetbot.IsAtThrowingDistance(targetBoxContainer))
                        {
                            Yeetbot.GotoState<YeetbotThrowBackwardsState>();
                        }
                    }
                    else
                    {
                        if (UnityEngine.Random.value > 0.3f)
                        {
                            Yeetbot.GotoState<YeetbotThrowBackwardsState>();
                        }
                        else
                        {
                            Yeetbot.GotoState<YeetbotTurnAroundState>();
                        }
                    }
                }
                else
                {
                    var targetBox = Yeetbot.ClosestBoxInSight;

                    if (targetBox && Yeetbot.IsAtArmsLength(targetBox))
                    {
                        Yeetbot.GotoState<YeetbotPickUpBoxState>();
                    }
                    else if (targetBox)
                    {
                        Yeetbot.GotoState<YeetbotWalkState>();
                    }
                    else if (Yeetbot.World.AvailableBoxes.Count() > 0)
                    {
                        Yeetbot.GotoState<YeetbotTurnAroundState>();
                    }
                }
            }
        }
    }
}