﻿using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;

namespace Iggi.AiDemos.Yeetbot
{
    /// <summary>
    /// Yeetbot Avatar class handles physcis-based movement and animation of body parts
    /// </summary>
    public class YeetbotAvatar : MonoBehaviour
    {
        #region Unity Inspector Properties

        [Header("Main")]
        [SerializeField] private Yeetbot yeetbot = default;

        [Header("Body Parts")]
        public Transform upperBody;
        public Transform middleBody;
        public Transform lowerBody;
        public Transform upperArm;
        public Transform lowerArm;
        public Transform upperArmMuscle;
        public Transform lowerArmMuscle;
        public Transform hand;
        public Transform eye;
        public ChainTrack chainTrack;

        #endregion

        #region Physics-based properties

        public SpringFloat bodySpring = new SpringFloat(friction: 1f);
        public SpringFloat armSpring = new SpringFloat(friction: 2f);
        public SpringFloat armFlex = new SpringFloat(dampening: 8f);

        public SpringFloat chainTrackLeftRadius = new SpringFloat(dampening: 1f, friction: 2f);
        public SpringFloat chainTrackRightRadius = new SpringFloat(dampening: 1f, friction: 2f);
        public SpringFloat chainTrackLength = new SpringFloat(dampening: 6f, friction: 2f);

        public SpringFloat movementVelocity = new SpringFloat(dampening: 8f, friction: 20f);
        public SpringFloat eyePosition = new SpringFloat(dampening: 8f, friction: 2f);

        #endregion

        #region Poses

        public float IdlePoseArmOffset => 0.542f;
        public float IdlePoseArmAngle => yeetbot.Facing.GetAngle(yeetbot.IsCarryingBox ? -25f : -65f, 90f, -90f);
        public float IdlePoseArmFlex => yeetbot.Facing.GetFloat(yeetbot.IsCarryingBox ? 0.35f : 0.55f);
        public float IdlePoseSmallWheelRadius => 0.10f;
        public float IdlePoseBigWheelRadius => 0.15f;
        public float IdlePoseChainTrackLength => 0.5f;
        public float IdlePoseLowerBodyOffset => 0.184f;
        public float IdlePoseMiddleBodyOffset => 0.236f;
        public float IdlePoseUpperBodyOffset => 0.182f;

        public float SleepPoseArmOffset => 0f;
        public float SleepPoseArmAngle => yeetbot.Facing.GetFloat(-90f);
        public float SleepPoseArmFlex => 1f;
        public float SleepPoseSmallWheelRadius => 0.10f;
        public float SleepPoseBigWheelRadius => 0.10f;
        public float SleepPoseChainTrackLength => 0f;
        public float SleepPoseLowerBodyOffset => 0f;
        public float SleepPoseMiddleBodyOffset => 0f;
        public float SleepPoseUpperBodyOffset => 0.114f;

        #endregion

        private Vector2 positionLastFrame;
        private float velocityLastFrame;

        public Vector3 EyeOffset => new Vector3(0.31f - yeetbot.Facing.GetFloat(0.01f), 0.29f, 1f);
        public float ArmLength => Vector2.Distance(upperArm.position, lowerArm.position) + Vector2.Distance(lowerArm.position, hand.position);

        protected void Start()
        {
            positionLastFrame = yeetbot.PhysicsBody.position;
            velocityLastFrame = 0f;
        }

        protected void LateUpdate()
        {
            var distanceMovedLastFrame = yeetbot.PhysicsBody.position - positionLastFrame;
            velocityLastFrame = distanceMovedLastFrame.magnitude * Mathf.Sign(distanceMovedLastFrame.x);
            positionLastFrame = yeetbot.PhysicsBody.position;

            movementVelocity.AddForce(armSpring.Velocity * 0.02f, ForceMode2D.Force);
            movementVelocity.Update();

            bodySpring.AddForce(velocityLastFrame * 10000f, ForceMode2D.Force);
            bodySpring.Update();

            armSpring.Update();
            armFlex.Update();

            yeetbot.Avatar.lowerBody.SetAngle(bodySpring * 3f);
            yeetbot.Avatar.middleBody.SetAngle(bodySpring * -1f);
            yeetbot.Avatar.upperBody.SetAngle(bodySpring * -1f);

            yeetbot.Avatar.upperArm.SetAngle(armSpring);
            yeetbot.Avatar.lowerArm.SetAngle(180f * armFlex);

            chainTrackLeftRadius.Update();
            chainTrackRightRadius.Update();
            chainTrackLength.Update();

            chainTrack.SetWheelProperties(chainTrackLeftRadius, chainTrackRightRadius, chainTrackLength);
            chainTrack.Distance += velocityLastFrame * -4.3f;

            eyePosition.Update();
            eye.localPosition = Vector3.Scale(EyeOffset, new Vector3(eyePosition.Value, 1f, 1f));
            eye.localScale = eye.localPosition.x > 0f ? Vector3.one : new Vector3(-1f, 1f, 1f);
        }

        public void ResetPose()
        {
            armSpring.Target = IdlePoseArmAngle;
            armFlex.Target = IdlePoseArmFlex;

            ResetChainTrack();
        }

        public void ResetChainTrack()
        {
            chainTrackRightRadius.Target = yeetbot.Facing.Choose(IdlePoseBigWheelRadius, IdlePoseSmallWheelRadius);
            chainTrackLeftRadius.Target = yeetbot.Facing.Choose(IdlePoseSmallWheelRadius, IdlePoseBigWheelRadius);
            chainTrackLength.Target = IdlePoseChainTrackLength;
        }

        public void MoveHandTowardsPoint(Vector2 worldPosition)
        {
            var armToPos = worldPosition - (Vector2)upperArm.position;
            var armToHand = (Vector2)hand.position - (Vector2)upperArm.position;
            var armToElbow = (Vector2)lowerArm.position - (Vector2)upperArm.position;

            var targetAngle = Vector2.SignedAngle(Vector2.right, armToPos) - yeetbot.Facing.GetFloat(Vector2.Angle(armToElbow, armToHand));
            var flexDiff = armToHand.magnitude - armToPos.magnitude;

            armFlex.Target += yeetbot.Facing.GetFloat(flexDiff * Time.smoothDeltaTime * 3f);
            armSpring.Target = MathUtil.NormalizeAngle(targetAngle, -90);
        }
        
        private void OnCollisionEnter2D(Collision2D collision)
        {
            if (collision.collider.attachedRigidbody == null) return;

            // Make Yeetbot flail if the box collides with him at high velocity
            if (collision.collider.attachedRigidbody.GetComponent<Box>() && (yeetbot.CurrentState is YeetbotSleepState) == false)
            {
                Flail(collision.collider.transform.position - upperArm.transform.position, collision.relativeVelocity);
            }

            // Make Yeetbot move away from container
            if (collision.collider.attachedRigidbody.GetComponent<BoxContainer>())
            {
                var direction = yeetbot.IsFacing(collision.collider) ? 1f : -1f;

                movementVelocity.Velocity = 0f;
                movementVelocity.AddForce(yeetbot.Facing.GetFloat(-30f) * direction, ForceMode2D.Impulse);
            }
        }

        private void Flail(Vector2 relativePosition, Vector2 relativeVelocity)
        {
            var force = -math.sign(relativePosition.x) * relativeVelocity.sqrMagnitude * 0.1f * Mathf.Sign(relativePosition.y + 0.3f);
            bodySpring.AddForce(force, ForceMode2D.Impulse);
        }
    }
}
