# AI Demos

Collection of small demos on how to use some common ai behaviours in dynamic environments.
 - [Play In Browser](https://noopol.com/ai-demos/player.html)
 - [Download Windows Build](https://noopol.com/ai-demos/AI_Demos_WinStandalone.rar)


## Demo #1 - Yeetbot

![enter image description here](https://media.giphy.com/media/XHjWZQ710vZTeqPKDu/giphy.gif)

Physics-based autonomous robot that organizes boxes in matching containers. There is no pre-made animations, the movements are 100% procedural. The robots AI is split into three components, the body, the eye, and the brain.

 - Body is just a set of physical joints, in this case mostly springs
 - Eye is a input sensor that collects data about its environment
 - The brain, powered by a Finite State Machine, controls the body based on eyes input data

Spring motion equations were inspired by Keijiro's [Klak](https://github.com/keijiro/Klak) library.


## Demo #2 - Labyrinth

![](https://media.giphy.com/media/dt12b0enyU4suwGE6K/giphy.gif)

Turtle that can find most optimal path between two points in a large. Its an general implementation of an [A* search algorithm](https://en.wikipedia.org/wiki/A*_search_algorithm), with the fast [Priority Queue](https://en.wikipedia.org/wiki/Priority_queue) at its core.
My implementation is pretty general, and a good starting point for many different projects. It can be further optimised on a case-by-case basis.

BombTurtle was made by [TomSka](https://www.youtube.com/TomSka)


## Licence
This project is licensed under the MIT License - see the [LICENSE.md](https://gitlab.com/manwhocanfly/ai-demos/blob/master/LICENCE.md) file for details

